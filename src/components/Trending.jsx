import styled from 'styled-components';
import '@splidejs/react-splide/css';
import { useEffect, useState } from "react";
import { Splide ,SplideSlide } from '@splidejs/react-splide';

export default function Trending() {

  const [trending, setTrending] = useState([]);
  
  
  useEffect(() => {
      getTrending();
    }, []); 
  
    const getTrending = async () => {
         const url = `${process.env.REACT_APP_URL_API}random?apiKey=${process.env.REACT_APP_API_KEY}&number=9`;
         const api = await fetch (url);
         const data = await api.json();
         setTrending(data.recipes) 
         
    }; 

      return (

        <Wrapper>

          <h3>Trending Picks</h3>
        <Splide
      options={{
        perPage: 5,
        arrows: false,
        pagination: false,
        drag: "free",
        gap: "3rem",
      }}
      >
        
          {trending.map((recipe) => {
            return (
              <SplideSlide>
             

                <Card>
                    <p>{recipe.title}</p>
                    <img src={recipe.image} alt={recipe.title} />   
                </Card>
              </SplideSlide>
            );
          })}
          </Splide>
      </Wrapper>
      
    );
    }

    
    const  Wrapper = styled.div`
    margin: 4rem 0rem;
    `;
    const Card = styled.div`
    min-height: 20rem;
    border-radius: 1.5rem;
    overflow: hidden;
    position: relative;
    img {
      border-radius: 1.5rem;
      position: absolute;
      left: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    p {
      position: absolute;
      z-index: 10;
      left: 50%;
      bottom: 0%;
      transform: translate(-50%, 0%);
      color: white;
      width: 100%;
      text-align: center;
      font-weight: 600;
      font-size: 1rem;
      height: 40%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    `;
    


