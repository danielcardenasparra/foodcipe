import { FaPizzaSlice, FaHamburger,FaUmbrellaBeach } from "react-icons/fa";
import { BsFillPersonPlusFill} from "react-icons/bs";
import { GiNoodles, GiForkKnifeSpoon } from "react-icons/gi";
import { NavLink} from "react-router-dom";
import styled from "styled-components";

export default function Category() {
  return (
    <List>
      <SLink to={"/login"}>
        <BsFillPersonPlusFill />
        <h4>Login</h4>
      </SLink>
      <SLink to={"/searched/American"}>
        <FaHamburger />
        <h4>American</h4>
      </SLink>
      <SLink to={"/searched/Caribbean"}>
        <FaUmbrellaBeach />
        <h4>Caribbean</h4>
      </SLink>
      <SLink to={"/searched/Italian"}>
        <FaPizzaSlice />
        <h4>Italian</h4>
      </SLink>
      <SLink to={"/searched/Thai"}>
        <GiNoodles />
        <h4>Thai</h4>
      </SLink>
      <SLink to={"/searched/Indian"}>
        <GiForkKnifeSpoon />
        <h4>Indian</h4>
      </SLink>

    </List>
  );
}

const List = styled.div`
  display: flex;
  flex-direction: column; 
  justify-content: center;
  
  margin:left:10rem; 
  margin-top: 30rem;
`;

const SLink = styled(NavLink)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  margin-top:2rem;
  margin-right: 1rem;
  text-decoration: none;
  background: linear-gradient(35deg,#c83131fa,#4e4040);
  width: 6rem;
  height: 6rem;
  cursor: pointer;
  transform: scale(0.8);
  h4 {
    color: white;
    font-size: 0.8rem;
    padding: 0.5rem;
  }
  svg {
    color:white;
    fill: #ffffff;
    font-size: 1.5rem;
  }
  &.active {
    background: linear-gradient(to right, #dcf0fb, #a55929);
    svg {
      fill: black;
    }
    h4 {
      color: white;
    }
  }
  &.hover {
    opacity: 0.5;
  }
`;
 