import styled from "styled-components";
import React, { useState } from "react";
import { FaSearch } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

export default function Search() {
  const [input, setInput] = useState("");
  const navigate = useNavigate();

  const submitHandler = (e) => {
    e.preventDefault();
    navigate("/searched/" + input);
    setInput('');
  };

  return (
    <FormStyle onSubmit={submitHandler}>
      <div>
        <FaSearch />
        <input
          type="text"
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
      </div>
    </FormStyle>
  );
}
    
const FormStyle = styled.form`
margin: 2rem 5rem 5rem;
position: relative;
div {
  width: 100%;
  position: relative;
}
input {
  border: none;
  background: linear-gradient(90deg,rgb(212 214 215) 0%,rgb(107 63 70) 49%,rgb(69 4 12 / 97%) 100%);
  font-size: 1rem;
  color: white;
  padding: 1rem 3rem;
  border: none;
  border-radius: 1rem;
  outline: none;
  width: 100%;
}
svg {
  position: absolute;
  top: 50%;
  left: 0%;
  transform: translate(100%, -50%);
  color: white;
}
`;

