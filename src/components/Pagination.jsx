import React from 'react'
import styled from 'styled-components'

function Pagination( recipePerPage, currentPage,setCurrentPage, totalRecipes) {

    // const pageNumbers=[]
    
    //(Math.ceil (totalRecipes / recipePerPage))
    // for (let i = 1; i)

  return (
    <PagCentered>
  <PrevPag>Previous</PrevPag>
  <List>
    <li><PagLink>1</PagLink></li>
    <li><PagLinkC>46</PagLinkC></li>
  </List>
  <NextPag >Next page</NextPag>
</PagCentered>
  )
}

const PagCentered = styled.nav`
display:flex;
flex-direction:column:
justify-content:center;
`;
const PrevPag = styled.a ``;

const NextPag = styled.a``;

const List = styled.ul``;

const PagLinkC = styled.a``;

const PagLink = styled.a`
`;
export default Pagination