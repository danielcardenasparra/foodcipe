import React from "react";
import styled from "styled-components";

const Header = () => {
  return (
    <Wrapper>
      <h1>You ready to cook? </h1>
      <p>
        Search the better recipes to enjoy with your family and friends.
        Be the next Gusteu with our App
      </p>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 0rem 2rem 2rem 2rem;
  margin-top: 5rem; 
  text-align: center;
  h1 {
    letter-spacing: 0.5rem;
  }
  p {
    padding: 1rem 4rem;
    font-size: 1.2rem;
  }
`;

export default Header;