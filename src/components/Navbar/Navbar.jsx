import React, { useState } from "react";
import { GiSpellBook } from "react-icons/gi";
import {
  Container,
  Nav,
  Logo,
  Wrapper,
  Menu,
  MenuItem,
  MenuItemLink,
  MobileIcon,
} from "./NavbarElements";
import {FaBars,FaTimes} from "react-icons/fa";
import Category from "../Category";

const Navbar = () => {
  const [showMenu, setShowMenu] = useState(false);

  return (
    <Container>
      <Wrapper>
        <Nav>
          <GiSpellBook />
          <Logo to={"/"}>FoodCipe</Logo>
        </Nav>
        
          <MobileIcon onClick={() => setShowMenu(!showMenu)}>
            {showMenu ? <FaTimes /> : <FaBars />}
          <Menu open = {showMenu}>
            <MenuItem>
              <MenuItemLink onClick={() => setShowMenu(!showMenu)}>
                <div>
                  <Category/>
                </div>
              </MenuItemLink>
            </MenuItem>
          </Menu>
        </MobileIcon>
      </Wrapper>
    </Container>
  );
};

export default Navbar;