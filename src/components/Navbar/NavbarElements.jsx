import styled from "styled-components";
import { Link } from "react-router-dom";



export const Container = styled.div`
width: 100%;
height: 70px;
margin-left: 0.8rem;
padding: 3rem 0rem 5rem 0rem;
display: flex;
justify-content: flex-start;
align-items: center;
border: 1px #50141b;
border-bottom-style: solid  ;
svg {
  font-size: 1.5rem;
`;

export const Logo = styled(Link)`
  text-decoration: none;
  font-size: 2rem;
  font-weight: 400;
  font-family: "Fuzzy Bubbles", regular400;
  margin-left: 0.8rem;
`;
export const MobileIcon = styled.div`
    display: flex;
    align-items: end;
    cursor: pointer;
    svg {
      fill: rgb(69 4 12);
      margin-right: 0.5rem;
    }
  }
`;
export const Nav = styled.div`
  }
`;
export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: auto;
`;

export const Menu = styled.ul`
width: 10rem;
height: 10rem;
display: flex;
flex-direction: column;
justify-content: space-between;
list-style: none;
background-color: #fff;
position: absolute;
top: 100px;
left: ${({ open }) => (open ? "flex" : "-100%")}; //Import
align-items: center;
  }
`;

export const MenuItem = styled.li`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  
`;

export const MenuItemLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 0.5rem 2.5rem;
  font-family: sans-serif;
  font-size: 1rem;
  font-weight: 300;
  cursor: pointer;
  transition: 0.5s all ease;
`;

