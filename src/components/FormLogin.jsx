import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import styled from "styled-components";

export default function FormLogin() {
  const [email, setEmail] = useState(localStorage.getItem("emailData"));
  const [password, setPassword] = useState(
    localStorage.getItem("passwordData")
  );
  const navigate = useNavigate();
  const handleSubmit = (event) => {
    if (email === "admin@foodcipe.com" && password === "recipes123") {
      localStorage.setItem("emailData", "admin@foodcipe.com");
      localStorage.setItem("passwordData", "recipes123");
      navigate("/");
    } else {
      event.preventDefault();
      alert("Incorrect password");
    }
  };

  return (
    <div>
      {
        <FormStyle onSubmit={handleSubmit}>
          <div>
            <Box>
              <Head>Login in to your account</Head>
              <Text>
                to enjoy the better <Nlink to={"/"}>recipes</Nlink>
              </Text>
            </Box>
            <FormBox>
              <div>
                <label for="username">Email Address</label>
                <input
                  type="text"
                  value={email}
                  placeholder="admin@foodcipe.com"
                  onChange={(e) => setEmail(e.target.value)}
                />

                <label for="password">Password</label>
                <input
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="*********"
                  
                />
                <button>Login</button>
              </div>
                
            </FormBox>
          </div>
        </FormStyle>
      }
    </div>
  );
}

const FormStyle = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  background: #ffff;
`;
const Box = styled.div`
  text-align: center;
  margin: 2rem; ;
`;
const Head = styled.p`
  font-size: 4em;
  color: #ae3737;
  font-weight: bold; ;
`;
const Text = styled.p`
  font-size: 2em;
`;
const Nlink = styled(Link)`
  color: #ae3737;
`;
const FormBox = styled.div`
  
  text-align:center;  
  width: 100%;
  height: 100%;
  border-radius: 30px;
  background: #fff;
  right:20px;
  div {
    width: 10rem;
    height: 10rem;
    margin:0px auto;
  }
  label {
    margin: 0;
    padding: 0;
    display: block;
    font-size: 20px;
  }
  input {
    border: none;
    outline: none;
    width:300px;
    height: 40px;
    background: #761111;
    color: #fff;
    font-size: 20px;
    border-radius: 15px;
  }
  button {
    margin: 2em 6rem;
    height: 40px;
    background: #761111;
    color: #fff;
    font-size: 18px;
    border-radius: 15px;
  }
`;
