import { useEffect, useState } from "react";
import styled from 'styled-components'; 
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/react-splide/css';

export default function Vegan() {
    const [vegan, setVegan] = useState([]);
  
    useEffect(() => {
      getVegan();
    }, []);


        const getVegan = async () => {

            const check = localStorage.getItem('vegan');
            if (check) {
                setVegan(JSON.parse(check));
            } else {
              const url = `${process.env.REACT_APP_URL_API}random?apiKey=${process.env.REACT_APP_API_KEY}&number=9`;
              const api = await fetch (url);
              const data = await api.json();
    
              localStorage.setItem('vegan', JSON.stringify(data.recipes))
              setVegan(data.recipes)
  };
            }
  return (
    <Wrapper>
    <h3>Our Vegan Trending Recipes</h3>
    <Splide
      options={{
        perPage: 3,
        arrows: false,
        pagination: false,
        drag: "free",
        gap: "3rem",
      }}
    >
      {vegan.map((recipe) => {
        return (
          <SplideSlide key={recipe.id}>
            <Card>
              
                <p>{recipe.title}</p>
                <img src={recipe.image} alt={recipe.title} />
                <Gradient />
              
            </Card>
          </SplideSlide>
        );
      })}
    </Splide>
  </Wrapper>
);
}

const Wrapper = styled.div`
margin: 4rem 0rem;
`;

const Card = styled.div`
min-height: 15rem;
min-width: 20rem;
border-radius: 1.5rem;
overflow: hidden;
position: relative;
img {
  border-radius: 1.5rem;
  position: absolute;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
}
p {
  position: absolute;
  z-index: 10;
  left: 50%;
  bottom: 0%;
  transform: translate(-50%, 0%);
  color: white;
  width: 100%;
  text-align: center;
  font-weight: 600;
  font-size: 1rem;
  height: 40%;
  display: flex;
  justify-content: center;
  align-items: center;
}
`;

const Gradient = styled.div`
z-index: 3;
position: absolute;
width: 100%;
height: 100%;
background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5));
`;
