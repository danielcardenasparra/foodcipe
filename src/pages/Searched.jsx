import React from "react";
import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import styled from "styled-components";
import Pagination from "../components/Pagination";
import Trending from "../components/Trending";
import Vegan from "../components/Vegan";


export default function Searched() {
  const [recipePerPage, setRecipePerPage] = useState(6);  
  
  const [currentPage, setCurrentPage] = useState(1);  
  const [searchedRecipes, setSearchedRecipes] = useState([]);
  const params = useParams();

  const getSearched = async (name) => {
    const data = await fetch(
      `${process.env.REACT_APP_URL_API}/complexSearch?apiKey=${process.env.REACT_APP_API_KEY}&query=${name}&number=20`
    );
    const recipes = await data.json();
    setSearchedRecipes(recipes.results);
  };
  
  const totalRecipes = getSearched.length

  useEffect(() => {
    getSearched(params.search);
    
  }, [params.search]);


  return (
    <>
       <Grid>
       {searchedRecipes.map((item) => {
         return (
           <Card key={item.id}>
             <Link to={"/recipe/" + item.id}>
               <img src={item.image} alt={item.title} />
               <h4>{item.title}</h4>
               
             </Link>
           </Card>
         
         
         );
       })}
     </Grid>
     <div>
      <Trending searchedRecipes={searchedRecipes}/>
      <Vegan searchedRecipes={searchedRecipes}/>
      </div>
     <agination recipePerPage={recipePerPage} currentPage={currentPage}  setCurrentPage={setCurrentPage} totalRecipes={totalRecipes} />
     </>
  );
}

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(20rem, 1fr));
  grid-gap: 3rem;
  
`;
const Card = styled.div`
margin:2rem 2rem;  
img {
    width: 10;
    border-radius: 2rem;
    
  }
  a {
    text-decoration: none;
  }
  h4 {
    text-align: center;
    padding: 1rem;
  }
`;
