import Trending from "../components/Trending";
import Vegan from "../components/Vegan";

export default function Home() {
  return (
    <div>
      <Trending/>
      <Vegan/>
      </div>
  );
}
