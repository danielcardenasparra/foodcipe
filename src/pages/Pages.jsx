import React from "react";
import Home from "./Home";
import { Routes, Route, useLocation } from "react-router-dom";
import Searched from "./Searched";
import Recipe from "./Recipe";
import FormLogin from "../components/FormLogin";
 

export default function Pages() {
  const location = useLocation();

  return (
    <div>
        

        <Routes location={location} key={location.pathname}>
          <Route path="/" exact element={<Home />} />
          <Route path="/searched/:search" element={<Searched />} />
          <Route path="/recipe/:name" element={<Recipe />} /> 
          <Route path="/login" element={<FormLogin />} />
        </Routes>
    
      
    </div>
  );}