import { BrowserRouter, Link } from "react-router-dom";
import { GiSecretBook } from "react-icons/gi";
import styled from "styled-components";
import Navbar from "./components/Navbar/Navbar";
import Search from "./components/Search";
import Header from "./components/Header";
import Pages from "./pages/Pages";



export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar>
          <GiSecretBook/>
          <Logo to={"/"}>Foodcipe</Logo>
        </Navbar>
        <Header/>
        <Search/>
        <Pages />
      </BrowserRouter>
    </div>
  );
}

const Logo = styled(Link)`
  text-decoration: none;
  font-size: 4rem;
  font-weight: 400;
  font-family: "Lobster Two", cursive;
  margin-left: 0.8rem;
`;

