# 🍲 FOODCIPE APP 🍲

This application allows users to search for recipes according to their preferences in an easy and simple way. The application includes the following functionalities:


- Search for recipes
- Display of trending recipes
- Listing and sorting of recipes
- Display of recipe details

---
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filen
ames include the hashes.\
Your app is ready to be deployed!

---

## `Technologies` 


## `create-react-app` <img src="https://img.icons8.com/office/30/000000/react.png"/>

- Provides development environment to start development.
- Provides fast refresh to speed up development.

## `styled components` <img src="https://img.icons8.com/emoji/30/000000/nail-polish-.png"/>

- Allows components to be styled independently and modularly
- Allows conditional styling based on component state

## `react-router-dom` <img src="https://img.icons8.com/ios-glyphs/30/000000/router--v1.png"/>

- Allows to navigate between the different pages of the application and provides an simple to understand structure.

## `react-icons` <img src="https://img.icons8.com/nolan/30/react-native.png"/>
- Diversity and easy integration
- The most used icons

## `react-splide` <img src="https://img.icons8.com/external-wanicon-two-tone-wanicon/30/000000/external-slide-kindergarten-wanicon-two-tone-wanicon.png"/>

- Allows you to create a slider quickly and easily to enhance the user experience.

## `dotenv` <img src="https://img.icons8.com/ios-glyphs/30/000000/outgoing-data.png"/>

- Allows to store in a safe way the environment variables used by the application. For example: recipe api url, api key to consume recipe api.
---
## `Screenshots` 

### Header 
![Header](https://user-images.githubusercontent.com/73352401/197373056-8669538f-916a-442d-8076-5a3dc90d0969.png)

---

### Navbar 
![Navb Menu](https://user-images.githubusercontent.com/73352401/197373102-ad976e70-4ad1-44b5-bc41-67738608b111.png)

---

### Login 
![Login](https://user-images.githubusercontent.com/73352401/197373122-bbe8b4d2-c631-419c-aebe-4ba5622af8b1.png)

---

###
### Trendings
![Home Trending Pick](https://user-images.githubusercontent.com/73352401/197373077-a5591662-85c6-40b9-84e2-41cafc80f237.png)

---

### Searchs 
![Menu Grid](https://user-images.githubusercontent.com/73352401/197373151-e57d8e79-7632-48e8-9154-c6449932d94f.png)

---

### Recipes
![Recipes](https://user-images.githubusercontent.com/73352401/197373163-6878f4e2-b069-4728-bab1-7635edf95164.png)
